import { Flight } from './../flight';
export class MockData {

  public static ExampleData: Flight[] =
  [
    {
      fullName: "phakkapon chintoo",
      from: "BKK",
      to: "LA",
      type: "One way",
      adults: 1,
      departure: new Date("2022-03-05"),
      children: 0,
      infants: 0,
      arrival: new Date("2022-03-06")
    }
  ]
}
